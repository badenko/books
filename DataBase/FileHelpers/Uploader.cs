﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;

namespace DataBase.FileHelpers
{
    public static class Uploader
    {
        public static void CreateOrder()
        {
            var books = LoadBooks();
            XmlDocument doc = new XmlDocument();
            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            XmlElement orders = doc.CreateElement("Orders");
            doc.AppendChild(orders);

            XmlElement orderNumber = doc.CreateElement("OrderNumber");
            XmlText root2Text = doc.CreateTextNode($"Order_{Date()}");
            orderNumber.AppendChild(root2Text);
            orders.AppendChild(orderNumber);

            XmlElement docDate = doc.CreateElement("DocDate");
            XmlText root3Text = doc.CreateTextNode($"{Date()}");
            docDate.AppendChild(root3Text);
            orders.AppendChild(docDate);

            XmlElement orderElements = doc.CreateElement("OrderElements");
            orders.AppendChild(orderElements);

            foreach (Book b in books)
            {
                XmlElement orderElement = doc.CreateElement("OrderElement");
                orderElements.AppendChild(orderElement);

                XmlElement bookName = doc.CreateElement("Book_Name");
                XmlText bookNameText = doc.CreateTextNode($"{b.Book_Name}");
                bookName.AppendChild(bookNameText);
                orderElement.AppendChild(bookName);

                XmlElement writerFirstName = doc.CreateElement("Writer_FirstName");
                XmlText writerFirstNameText = doc.CreateTextNode($"{b.Writer_FirstName}");
                writerFirstName.AppendChild(writerFirstNameText);
                orderElement.AppendChild(writerFirstName);

                XmlElement writerLastName = doc.CreateElement("Writer_LastName");
                XmlText writerLastNameText = doc.CreateTextNode($"{b.Writer_LastName}");
                writerLastName.AppendChild(writerLastNameText);
                orderElement.AppendChild(writerLastName);

                XmlElement bookDetails = doc.CreateElement("Book_details");
                orderElement.AppendChild(bookDetails);

                XmlElement publishDate = doc.CreateElement("Publishdate");
                XmlText publishDateText = doc.CreateTextNode($"{b.Publishdate.ToString("yyyy-MM-dd")}");
                publishDate.AppendChild(publishDateText);
                bookDetails.AppendChild(publishDate);

                XmlElement pagePrice = doc.CreateElement("PagePrice");
                XmlText pagePriceText = doc.CreateTextNode($"{Math.Round((b.Price / b.Page_quantity), 3).ToString(CultureInfo.InvariantCulture.NumberFormat)}");
                pagePrice.AppendChild(pagePriceText);
                bookDetails.AppendChild(pagePrice);
            }
            doc.Save($"{ConnectionString()}");

        }

        public static void ShowOrder()
        {
            var books = LoadBooks();
            foreach (Book b in books)
            {
                decimal ppp = b.Price / b.Page_quantity;
                ppp = Math.Round(ppp, 5);
                Console.WriteLine($"{b.Id} {b.Book_Name} {ppp} {b.Publishdate.ToString("yyyy-MM-dd")}");
            }
            Console.ReadKey();
        }

        
        private static string DirectoryChecker()
        {
            Config conf = Config.Instance;
            string DirPath = conf.UploadPath;// ConfigurationManager.AppSettings["uploadpath"];
            if (!Directory.Exists(DirPath)) Directory.CreateDirectory(DirPath);
            return DirPath;
        }
        private static string ConnectionString()
        {
            Config conf = Config.Instance;
            var configPath = conf.UploadPath; //ConfigurationManager.AppSettings["uploadPath"];
            return $"{configPath}\\{FileName()}";
        }
        private static string FileName()
        {
            return $"Order_{Date()}.xml";
        }
        private static string Date()
        {
            int year = DateTime.Now.Year;
            int month = DateTime.Now.Month;
            int day = DateTime.Now.Day;

            string daychecker = day.ToString();
            string monthchecker = month.ToString();
            if (day < 10) daychecker = $"0{day}";
            if (month < 10) monthchecker = $"0{month}";

            return $"{year}{monthchecker}{daychecker}";
        }
        private static List<Book> LoadBooks()
        {
            Config conf = Config.Instance;
            int rating = conf.Rating; //Convert.ToInt32(ConfigurationManager.AppSettings["rating"]);
            using (AppContext db = new AppContext())
            {
                var books = (from b in db.Books
                             where b.Raiting >= rating 
                             orderby b.Price / b.Page_quantity
                             select b).Take(100).ToList();
                return books;
            }
        }
    }
}
