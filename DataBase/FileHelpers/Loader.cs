﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;

namespace DataBase.FileHelpers
{
    public static class Loader
    {
        public static void Load()
        {
            int counter = 0;
            var newBooks = Comparer();
            using (AppContext db = new AppContext())
            {
                foreach(Book b in newBooks)
                {
                    db.Books.Add(b);
                    counter++;
                }
                db.Logs.Add(new DB.Log() { TimeStamp = DateTime.UtcNow, Data = $"{FileName()} has been added. {counter} records updated" });
                db.SaveChanges();
            }
        }

        public static void Show()
        {
            using (AppContext db = new AppContext())
            {
                var books = db.Books;
                var logs = db.Logs;
                foreach(Book b in books)
                {
                    Console.WriteLine($"{b.Id}\t{b.Book_Name}\t{b.Publishdate}\t{b.Price}");
                }
                foreach(DB.Log l in logs)
                {
                    Console.WriteLine($"{l.Id}\t{l.TimeStamp}\t{l.Data}");
                }
                Console.ReadKey();
            }
        }

        private static List<Book> Comparer()
        {
            var newBooks = ParsedFromFile();
            using (AppContext db = new AppContext())
            {
                var books = db.Books;
                foreach(Book b in newBooks)
                {

                    var duplicateChecker = db.Books.FirstOrDefault(
                        _ => _.Book_Name == b.Book_Name 
                        && _.Writer_FirstName == b.Writer_FirstName);

                    if (duplicateChecker != null)
                    {
                        if (b.Writer_LastName != null || b.Writer_LastName != "") duplicateChecker.Writer_LastName = b.Writer_LastName;
                        if (b.Publishdate != null) duplicateChecker.Publishdate = b.Publishdate;
                        if (b.Page_quantity != null) duplicateChecker.Page_quantity = b.Page_quantity;
                        if (b.Raiting != null) duplicateChecker.Raiting = b.Raiting;
                        duplicateChecker.Price = b.Price;
                        db.SaveChanges();

                        newBooks.Remove(b);
                    }
                }
            }
            return newBooks;
        }
        private static List<Book> ParsedFromFile()
        {
            List<Book> books = new List<Book>();
            using (TextReader reader = new StreamReader(ConnectionString()))
            using (var csv = new CsvReader(reader))
            {
                var records = csv.GetRecords<BookModel>();
                foreach (BookModel b in records)
                {
                    decimal price = decimal.Parse(b.Price, new CultureInfo("en-US"));

                    Book a = new Book()
                    {
                        Book_Name = b.Book_Name,
                        Writer_FirstName = b.Writer_FisrtName,
                        Writer_LastName = b.Writer_LastName,
                        Publishdate = b.Publishdate,
                        Page_quantity = b.Page_quantity,
                        Raiting = b.Raiting,
                        Price = price
                    };
                    books.Add(a);
                }
            }
            return books;
        }
        private static string ConnectionString()
        {
            Config conf = Config.Instance;
            var configPath = conf.SourcePath; //ConfigurationManager.AppSettings["sourcePath"];
            return $"{configPath}{FileName()}";
        }
        private static string FileName()
        {
            int year = DateTime.Now.Year;
            int month = DateTime.Now.Month;
            int day = DateTime.Now.Day;

            string daychecker = day.ToString();
            string monthchecker = month.ToString();
            if (day < 10) daychecker = $"0{day}";
            if (month < 10) monthchecker = $"0{month}";

            string date = $"{year}{monthchecker}{daychecker}";
            return $"booklist_{date}.csv";
        }
        private class BookModel
        {
            public string Book_Name { get; set; }
            public string Writer_FisrtName { get; set; }
            public string Writer_LastName { get; set; }
            public DateTime Publishdate { get; set; }
            public int Page_quantity { get; set; }
            public int Raiting { get; set; }
            public string Price { get; set; }
        }
    }
}
