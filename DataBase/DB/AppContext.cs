﻿using System.Data.Entity;


namespace DataBase
{
    public class AppContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<DB.Log> Logs { get; set; }
    }
}
