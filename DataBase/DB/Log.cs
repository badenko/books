﻿using System;

namespace DataBase.DB
{
    public class Log
    {
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Data { get; set; }
    }
}
