﻿using System;

namespace DataBase
{
    public class Book
    {
        public int Id { get; set; }
        public string Book_Name { get; set; }
        public string Writer_FirstName { get; set; }
        public string Writer_LastName { get; set; }
        public DateTime Publishdate { get; set; }
        public int Page_quantity { get; set; }
        public int Raiting { get; set; }
        public decimal Price { get; set; }
    }
}
