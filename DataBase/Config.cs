﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace DataBase
{
    public sealed class Config
    {
        private static readonly Lazy<Config>
            lazy =
            new Lazy<Config>
                (() => new Config());

        public static Config Instance { get { return lazy.Value; } }

        private Config()
        {
            Settings settings = new Settings();
            JsonSerializer serializer = new JsonSerializer();
            if(!File.Exists("Settings.json"))
            {
                settings.SourcePath = Directory.GetCurrentDirectory();
                settings.UploadPath = Directory.GetCurrentDirectory();
                settings.Rating = 8;
                using (StreamWriter sw = new StreamWriter("Settings.json"))
                using (JsonWriter jw = new JsonTextWriter(sw))
                {
                    serializer.Serialize(jw, settings);
                }
                SourcePath = settings.SourcePath;
                UploadPath = settings.UploadPath;
                Rating = settings.Rating;
            }
            else
            {
                using (StreamReader sr = File.OpenText(("Settings.json")))
                {
                    settings = (Settings)serializer.Deserialize(sr, typeof(Settings));
                }
                SourcePath = settings.SourcePath;
                UploadPath = settings.UploadPath;
                Rating = settings.Rating;
            }
        }

        public string SourcePath { get; private set; }
        public string UploadPath { get; private set; }
        public int Rating { get; private set; }

        private class Settings
        {
            public string SourcePath { get; set; }
            public string UploadPath { get; set; }
            public int Rating { get; set; }
        }
    }
}
