﻿using DataBase.FileHelpers;
using System;

namespace DataBase
{
    class Program
    {
        static void Main(string[] args)
        {
            Config conf = Config.Instance;
            while(true)
            {
                Console.WriteLine(
                    "Enter 1 to Load File\n" +
                    "Enter 2 to Create Order\n" +
                    "Enter 3 to ShowAll\n" +
                    "Enter 9 to exit");
                int choice = 0;
                try
                {
                    choice = Convert.ToInt32((Console.ReadLine()));
                }
                catch(FormatException)
                {
                    Console.WriteLine("Your arguments are invalid press any button to continue...");
                    Console.ReadKey();
                }
                if (choice == 1) Loader.Load();
                if (choice == 2) Uploader.CreateOrder();
                if (choice == 3) Loader.Show();
                if (choice == 9) break;
                Console.Clear();
            }
        }
    }
}
