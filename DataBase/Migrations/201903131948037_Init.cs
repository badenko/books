namespace DataBase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Logs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TimeStamp = c.DateTime(nullable: false),
                        Data = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Books", "Writer_FirstName", c => c.String());
            AlterColumn("dbo.Books", "Publishdate", c => c.DateTime());
            AlterColumn("dbo.Books", "Page_quantity", c => c.Int());
            AlterColumn("dbo.Books", "Raiting", c => c.Int());
            DropColumn("dbo.Books", "Writer_FisrtName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Books", "Writer_FisrtName", c => c.String());
            AlterColumn("dbo.Books", "Raiting", c => c.Int(nullable: false));
            AlterColumn("dbo.Books", "Page_quantity", c => c.Int(nullable: false));
            AlterColumn("dbo.Books", "Publishdate", c => c.DateTime(nullable: false));
            DropColumn("dbo.Books", "Writer_FirstName");
            DropTable("dbo.Logs");
        }
    }
}
