
namespace DataBase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class blahblah : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Books", "Publishdate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Books", "Page_quantity", c => c.Int(nullable: false));
            AlterColumn("dbo.Books", "Raiting", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Books", "Raiting", c => c.Int());
            AlterColumn("dbo.Books", "Page_quantity", c => c.Int());
            AlterColumn("dbo.Books", "Publishdate", c => c.DateTime());
        }
    }
}
